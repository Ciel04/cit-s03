package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3444919613454783968L;
	private String contentType;

	public void init() throws ServletException{
		contentType = "index/html";
		System.out.println("***************************");
		System.out.println("Initialized Connection to DB");
		System.out.println("***************************");
	}
	public void destroy() {
		System.out.println("***************************");
		System.out.println("Disconnected from DB");
		System.out.println("***************************");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		System.out.println("Hello from the calculator servlet");
		int result = 0;
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		
		switch(operation) {
			case "add":
				result = num1 + num2;
				break;
			case "subtract":
				result = num1 - num2;
				break;
			case "multiply":
				result = num1 * num2;
				break;
			case "divide":
				result = num1 / num2;
				break;
		}
		
		PrintWriter out = res.getWriter();
		
		out.println("<p>The numbers you provided are: "+num1+", "+num2+"</p>");
		out.println("<p>The operation that you wanted is: "+operation+"</p>");
		out.println("<p>The result is: "+result+"</p>");

	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("<p>To use the app, input two numbers and an operation.</p>");
		out.println("<p>Hit the submit button after filling in the details.</p>");
		out.println("<p>You will get the result shown in your browser!</p>");
	}
}
